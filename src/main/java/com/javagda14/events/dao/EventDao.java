package com.javagda14.events.dao;

import com.javagda14.events.model.Event;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.Transaction;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.List;

@LocalBean
@Singleton
public class EventDao {
    public void save(Event event){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(event);

            transaction.commit();
        } catch (SessionException sessionException) {
            transaction.rollback();
            System.out.println("Error");
        }
    }

    public List<Event> getAllEvents(){
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List<Event> events = session.createQuery("from Event", Event.class).list();

            return events;
        } catch (SessionException sessionException) {
            System.out.println("Error");
        }
        return new ArrayList<>();
    }

}
