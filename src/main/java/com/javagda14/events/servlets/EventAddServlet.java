package com.javagda14.events.servlets;

import com.javagda14.events.model.AppUser;
import com.javagda14.events.model.Event;
import com.javagda14.events.service.EventService;
import com.javagda14.events.service.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.text.html.Option;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@WebServlet("/event/add")
public class EventAddServlet extends HttpServlet {

    // musimy powiązać sobie user service, ponieważ będziemy z niego (z dao) pobierali użytkownika o podanym id
    @EJB
    private UserService userService;

    @EJB
    private EventService eventService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session == null || session.getAttribute("logged_id") == null) {
            resp.sendRedirect(req.getContextPath() + "/");
            return;
        }

        req.getRequestDispatcher("/event/event_add.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // pobranie danych z parametrów
        String name = req.getParameter("name");
        String descr = req.getParameter("description");
        String length = req.getParameter("length");
        String time = req.getParameter("time");
        String owner_id = req.getParameter("owner_id");

        // Tworzę event z zadanych parametrów (dane z formularza)
        Event event = new Event();
        event.setDescription(descr);
        event.setLength(Integer.parseInt(length));
        event.setTime(LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));
        event.setName(name);
        // event nie ma jeszcze powiązania z userem

        // musimy odnaleźć zalogowanego użytkownika w bazie danych (szukamy po id)
        // szukamy go i jeśli go odnajdziemy to będzie w optionalu
        Optional<AppUser> appUserOptional = userService.findUserById(Long.parseLong(owner_id));
        if (appUserOptional.isPresent()) {
            // wyciągam ownera z optionala
            AppUser owner = appUserOptional.get();

            // ustawiamy w evencie owner'a
            event.setOwner(owner);

            // ustawiamy w ownerze event
            owner.getEventList().add(event);

            // należy zapisać zmiany w evencie
            eventService.save(event);

            // należy zapisać zmiany w ownerze
//            userService.update(owner);
        }

        resp.sendRedirect(req.getContextPath() + "/event/list");
    }
}
