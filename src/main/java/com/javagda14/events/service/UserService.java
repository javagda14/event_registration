package com.javagda14.events.service;


import com.javagda14.events.dao.UserDao;
import com.javagda14.events.model.AppUser;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import java.util.Optional;

@LocalBean
@Singleton
public class UserService {

    @EJB
    private UserDao userDao;

    public boolean checkUsernameNotExists(String username) {
        if (userDao.checkIfUsernameExists(username).isPresent()) {
            return false;
        }
        return true;
    }

    public boolean checkEmailNotExists(String email) {
        if (userDao.checkIfEmailExists(email).isPresent()) {
            return false;
        }
        return true;
    }

    public void register(AppUser appUser) {
        userDao.add(appUser);
    }

    public Optional<Long> login(String login, String password) {
        Optional<AppUser> appUserOptional = userDao.getUserWithLoginAndPassword(login, password);
        if (appUserOptional.isPresent()) {
            return Optional.of(appUserOptional.get().getId());
        }
        return Optional.empty();
    }

    public Optional<AppUser> findUserById(Long owner_id) {
        return userDao.findUserWithId(owner_id);
    }
}
