package com.javagda14.events.service;


import com.javagda14.events.dao.EventDao;
import com.javagda14.events.model.AppUser;
import com.javagda14.events.model.Event;
import com.javagda14.events.model.dto.EventDto;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.swing.text.html.Option;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@LocalBean
@Singleton
public class EventService {

    @EJB
    private EventDao eventDao;

    @EJB
    private UserService userService;

    public void save(Event event) {
        eventDao.save(event);
    }

    public List<Event> getEventList() {
        return eventDao.getAllEvents();
    }

    public Optional<Event> addNewEvent(EventDto eventDto) {
        // konwersja DTO - > entity (żeby móc zapisać obiekt Event w bazie danych)
        Event event = new Event();
        event.setName(eventDto.getName());
        event.setTime(LocalDateTime.parse(eventDto.getTime(), DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        event.setLength(eventDto.getLength());
        event.setDescription(eventDto.getDescription());

        // pobranie z dto owner_id
        Long owner_id = Long.parseLong(eventDto.getOwnerId());

        // znalezienie użytkownika z podanym owner_id
        Optional<AppUser> appUserOptional = userService.findUserById(owner_id);
        if (appUserOptional.isPresent()) {

            // ustawienie event'owi owner'a
            event.setOwner(appUserOptional.get());

            // zapis do bazy
            eventDao.save(event);

            // zwrócenie event'u w wyniku.
            return Optional.of(event);
        }

        return Optional.empty();
    }
}