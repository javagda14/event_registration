<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/8/18
  Time: 6:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Event Add</title>
</head>
<body>
<%@include file="/header.jsp" %>
<h2>Event form:</h2>
<form action="/event/add" method="post">
    <input name="owner_id" type="hidden" hidden value="<c:out value="${sessionScope.logged_id}"/>">
    <div>
        <label for="name">Name:</label>
        <input id="name" name="name" type="text">
    </div>
    <div>
        <label for="description">Description:</label>
        <input id="description" name="description" type="text">
    </div>
    <div>
        <label for="time">Time:</label>
        <input id="time" name="time" type="datetime-local">
    </div>
    <div>
        <label for="length">Length (in minutes):</label>
        <input id="length" name="length" type="number">
    </div>
    <input type="submit" value="Add">
</form>
</body>
</html>
