<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>User form</title>
</head>
<body>
<%@include file="/header.jsp" %>

<h2>Registration form:</h2>
<span style="color: red; ">
    <c:if test="${not empty error_message}">
        <c:out value="${error_message}"/>
    </c:if>
</span>

<form action="/user/register" method="post">
    <div>
        <label for="email">Email:</label>
        <input id="email" name="email" type="email">
    </div>
    <div>
        <label for="username">Username:</label>
        <input id="username" name="username" type="text">
    </div>
    <div>
        <label for="password">Password:</label>
        <input id="password" name="password" type="password">
    </div>
    <div>
        <label for="password-confirm">Password confirm:</label>
        <input id="password-confirm" name="password-confirm" type="password">
    </div>
    <input type="submit" value="Register">
</form>
</body>
</html>
