<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/8/18
  Time: 7:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<%@include file="/header.jsp" %>
<h2>Login form:</h2>
<form action="/login" method="post">
    <div>
        <label for="username">Username:</label>
        <input id="username" name="username" type="text">
    </div>
    <div>
        <label for="password">Password:</label>
        <input id="password" name="password" type="password">
    </div>
    <input type="submit" value="Login">
</form>
</body>
</html>
